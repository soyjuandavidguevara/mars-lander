import java.awt.Dimension
import javax.swing.*

class VisualDebugging: JFrame() {
    private val w = 700
    private val h = 300
    private val drawingBoard = Drawer(w, h)

    init {
        this.size = Dimension(w, h)
        this.title = "Mars Lander Simulator"

        this.add(drawingBoard)

        this.defaultCloseOperation = EXIT_ON_CLOSE
        this.isVisible = true
    }

    fun loadMap(map: SurfaceMap) {
        drawingBoard.loadMap(map, this.graphics)
    }
}