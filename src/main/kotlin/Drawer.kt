import java.awt.*
import java.awt.geom.*
import javax.swing.*

class Drawer(w: Int, h: Int) : JComponent() {
    private var width: Int = w
    private var height: Int = h
    private lateinit var g: Graphics2D

    override fun paintComponent(gr: Graphics) {
        println("Drawing")
        g = gr as Graphics2D
        g.color = Color.RED
        g.drawLine(0, 0, width, height)
    }

    fun loadMap(map: SurfaceMap, gr: Graphics) {
        for (i in map.getEdges())
            gr.drawLine(i.first.first, i.first.second, i.second.first, i.second.second)
    }
}