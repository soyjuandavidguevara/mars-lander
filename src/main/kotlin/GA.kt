import java.util.concurrent.ThreadLocalRandom
import kotlin.math.*
import kotlin.random.Random
import kotlin.system.measureTimeMillis

private operator fun <E> List<E>.component6(): E = this[5]
private operator fun <E> List<E>.component7(): E = this[6]
private fun randInt(from: Int, to: Int, till: Boolean = true) = ThreadLocalRandom.current().nextInt(from, if (till) to + 1 else to)


data class Ship(
    var x: Int = 0,
    var y: Int = 0,
    var vx: Int = 0,
    var vy: Int = 0,
    var degree: Int = 0,
    var power: Int = 0,
    var fuel: Int = 0,
) {
    override fun toString(): String = "Ship(x=$x, y=$y, vx=$vx, vy=$vy, degree=$degree, power=$power, fuel=$fuel)"
}

enum class Result {
    CRASH, LANDING, NOTHING, LOST, LANDING_CRASH
}

fun Result.isFinish(): Boolean {
    return this != Result.NOTHING
}

data class Action(var power: Int, var angle: Int) {
    override fun toString(): String = "$angle,$power"
}
data class Genome(val action: Action, val evaluation: Double, val result: Result) {
    fun mutate(): Genome {
        var newPower = this.action.power
        var newAngle = this.action.angle

        if (Random.nextDouble() < 0.1) {
            newPower += randInt(-1, 1)
            newAngle += randInt(-15, 15)
        }

        if (newPower > 4) newPower = 4
        if (newPower < 0) newPower = 0
        if (newAngle > 90) newAngle = 90
        if (newAngle < -90) newAngle = -90

        return Genome(Action(newPower, newAngle), 0.0, result)
    }
}

typealias Chromosome = MutableList<Genome>

fun Chromosome.evaluate() = this.sumOf { it.evaluation }

class ChromosomeComparator : Comparator<Chromosome> {
    override fun compare(a: Chromosome, b: Chromosome): Int {
//        val lastA = a.last()
//        val lastB = b.last()
//
//        if (lastA.evaluation < lastB.evaluation) return -1
//        else if (lastA.evaluation > lastB.evaluation) return 1
//
//        if (a.evaluate() / a.size < b.evaluate() / b.size) return -1
//        else if (a.evaluate() / a.size > b.evaluate() / b.size) return 1
//        return 0
        if (a.evaluate() / a.size > b.evaluate() / b.size) return -1
        else if (a.evaluate() / a.size < b.evaluate() / b.size) return 1
        return 0
    }
}

typealias Population = MutableList<Chromosome>
class Search {
    private val population: Population = mutableListOf()
    private lateinit var map: SurfaceMap
    private lateinit var ship: Ship

    fun init(map: SurfaceMap, initialShip: Ship) {
        this.map = map
        this.ship = initialShip
        val simulation = Simulation(map, initialShip)
        repeat(500) {
            val downAngle = max(-90, initialShip.degree - 15)
            val upAngle = min(90, initialShip.degree + 15)

            val downPower = max(0, initialShip.power - 1)
            val upPower = min(4, initialShip.power + 1)
            while (!simulation.getResult().isFinish()) {
                val angle = randInt(downAngle, upAngle)
                val power = randInt(downPower, upPower)
                simulation.nextState(angle, power)
                simulation.saveState()
            }
            population.add(simulation.history.toMutableList())
            simulation.reset()
        }
    }

    private fun selection(): Population {
        val selectedPopulation = mutableListOf<Chromosome>()
        val sortedPopulation = population.sortedWith(ChromosomeComparator())
        while (selectedPopulation.size < 48) {
            val selectedSubset = mutableListOf<Chromosome>()

            for (i in 0 until 5) {
                selectedSubset.add(sortedPopulation[randInt(0, population.size, false)])
            }

            selectedSubset.sortWith(ChromosomeComparator())
            selectedPopulation.add(selectedSubset.first())
        }

        selectedPopulation.add(0, sortedPopulation[1])
        selectedPopulation.add(0, sortedPopulation[0])
        return selectedPopulation
    }

    private fun crossover(parents: Population): Population {
        val parentA = parents[0]
        val parentB = parents[1]

        val newPopulation = mutableListOf<Chromosome>()

        while (newPopulation.size < 498) {
            val parentC = parents[randInt(0, parents.size, false)]
            val parentD = parents[randInt(0, parents.size, false)]

            val crossoverPoint1 = randInt(2, min(parentA.size, parentB.size), false)
            val crossoverPoint2 = randInt(2, min(parentC.size, parentD.size), false)

            val newChromosome1 = mutableListOf<Genome>()
            val newChromosome2 = mutableListOf<Genome>()
            val newChromosome3 = mutableListOf<Genome>()
            val newChromosome4 = mutableListOf<Genome>()


            newChromosome1 += parentA.slice(IntRange(0, crossoverPoint1 - 2))
            newChromosome1 += parentB.slice(IntRange(crossoverPoint1 - 1, parentB.size - 1))

            newChromosome2 += parentB.slice(IntRange(0, crossoverPoint1 - 2))
            newChromosome2 += parentA.slice(IntRange(crossoverPoint1 - 1, parentA.size - 1))

            newChromosome3 += parentC.slice(IntRange(0, crossoverPoint2 - 2))
            newChromosome3 += parentD.slice(IntRange(crossoverPoint2 - 1, parentD.size - 1))

            newChromosome4 += parentD.slice(IntRange(0, crossoverPoint2 - 2))
            newChromosome4 += parentC.slice(IntRange(crossoverPoint2 - 1, parentC.size - 1))

            newPopulation.add(newChromosome1)
            newPopulation.add(newChromosome2)
            newPopulation.add(newChromosome3)
            newPopulation.add(newChromosome4)
        }

        newPopulation.add(parentA)
        newPopulation.add(parentB)

        return newPopulation
    }

    private fun mutation(mutationTarget: Population): Population {
        val mutatedPopulation = mutableListOf<Chromosome>()

        for (i in mutationTarget.indices) {
            val newChromosome = mutationTarget[i]
            if (Random.nextDouble() < 0.1) {
                for (j in mutationTarget[i].indices) {
                    val newGenome = mutationTarget[i][j].mutate()
                    newChromosome[j] = newGenome
                }
            }
            mutatedPopulation.add(newChromosome)
        }

        return mutatedPopulation
    }

    private fun completion(population: Population, map: SurfaceMap, initialShip: Ship): Population {
        val completed = mutableListOf<Chromosome>()

        val simulation = Simulation(map, initialShip)
        for (chromosome in population) {
            completed.add(simulation.complete(chromosome))
        }

        return completed
    }

    fun improve() {
        var i = 0
        while (population.minWith(ChromosomeComparator()).last().result != Result.LANDING) {
            i++
            val selected = selection()
            val generated = crossover(selected)
            val mutated = mutation(generated)
            population.clear()
            population.addAll(completion(mutated, map, ship))
            print("\rBest Chromosome: ${population.minWith(ChromosomeComparator()).last().evaluation} - Gen: $i")
        }
    }

    fun analytics() {
        System.err.println("Chromosomes: ${population.size}\n")
        val sortedPopulation = population.sortedWith(ChromosomeComparator())
        val best = sortedPopulation[0]
        val worst = sortedPopulation[sortedPopulation.size - 1]
        System.err.println("Best Chromosome: ${best.evaluate()}\nLast Genome: ${best.last().evaluation}")
        System.err.println("Worst Chromosome: ${worst.evaluate()}\nLast Genome: ${worst.last().evaluation}")
    }

    fun printSolutions() {
        val sortedPopulation = population.sortedWith(ChromosomeComparator()).filter { it.last().result == Result.LANDING }
        sortedPopulation.forEach {
            val sol = mutableListOf<String>()
            it.forEach { g -> sol.add(g.action.toString()) }
            println(sol.joinToString(";"))
        }
    }
}


fun evaluation(map: SurfaceMap, ship: Ship, result: Result): Double {
    val landingPoint = map.getLandingPoint()
    return ship.fuel.toDouble()
//    return hypot((landingPoint.first - ship.x).toDouble(), (landingPoint.second - ship.y).toDouble()) + ship.fuel
//    return when(result) {
//        Result.LOST -> 50000.0
//        Result.CRASH -> {
//            30000.0 + hypot((landingPoint.first - ship.x).toDouble(), (landingPoint.second - ship.y).toDouble())
//        }
//        Result.NOTHING -> {
//            20000.0 + hypot((landingPoint.first - ship.x).toDouble(), (landingPoint.second - ship.y).toDouble())
//        }
//        Result.LANDING_CRASH -> {
//            10000.0 + abs(ship.vx) + abs(ship.vy) + abs(ship.degree)
//        }
//        Result.LANDING -> 1.0/ship.fuel
//    }
}

class Simulation(private val map: SurfaceMap, private val initialShip: Ship) {
    private var ship: Ship = initialShip.copy()
    private var lastAction = Action(-1, -1)
    val history = mutableListOf<Genome>()
    fun nextState(nextDegree: Int, nextPower: Int): Ship {
        lastAction.power = nextPower
        lastAction.angle = nextDegree
        val change = abs(nextDegree - ship.degree)
        val newDegree = if (change > 15) {
            if (nextDegree > ship.degree) ship.degree + 15 else ship.degree - 15
        } else nextDegree

        val powerChange = abs(nextPower - ship.power)
        val power = if (ship.fuel > 0) if (powerChange > 1) {
            if (nextPower > ship.power) ship.power + 1 else ship.power - 1
        } else nextPower else 0

        val aX: Double = power * cos(Math.toRadians((newDegree + 90).toDouble()))
        val aY: Double = -3.711 + (power * sin(Math.toRadians((newDegree + 90).toDouble())))

        val newX = ship.x + ship.vx + (0.5 * aX)
        val newY = ship.y + ship.vy + (0.5 * aY)

        val newVx = ship.vx + aX
        val newVy = ship.vy + aY

        this.ship.x = newX.roundToInt()
        this.ship.y = newY.roundToInt()
        this.ship.vx = newVx.roundToInt()
        this.ship.vy = newVy.roundToInt()
        this.ship.fuel = ship.fuel - power
        this.ship.power = power
        this.ship.degree = newDegree

        return this.ship
    }

    fun getResult(): Result {
        if (ship.x < 0 || ship.x > 6999 || ship.y > 2999) return Result.LOST
        if (!map.inTerrain(ship.x, ship.y)) return Result.NOTHING
        val landingPoint = map.getLandingPoint()
        if (ship.x < (landingPoint.first - 250) || ship.x > (landingPoint.first + 250)) return Result.CRASH
        if (ship.degree != 0) return Result.LANDING_CRASH
        if (abs(ship.vx) > 20) return Result.LANDING_CRASH
        if (abs(ship.vy) > 40) return Result.LANDING_CRASH
        return Result.LANDING
    }

    fun saveState() = history.add(Genome(lastAction.copy(), evaluation(map, ship, getResult()), getResult()))
    fun reset() {
        history.clear()
        ship = initialShip.copy()
        lastAction.angle = -1
        lastAction.power = -1
    }

    fun complete(chromosome: Chromosome): Chromosome {
        this.reset()
        for (genome in chromosome) {
            this.nextState(genome.action.angle, genome.action.power)
            this.saveState()
            if (this.getResult().isFinish()) break
        }

        if (!this.getResult().isFinish()) {
            val lastAction = chromosome.last().action
            while (!this.getResult().isFinish()) {
                this.nextState(lastAction.angle, lastAction.power)
                this.saveState()
            }
        }

        return this.history.toMutableList()
    }
}

fun main() {
    val map = SurfaceMap()
    map.addSurfacePoint(0, 0)
//    CH1
//    map.addSurfacePoint(0,100)
//    map.addSurfacePoint(1000,500)
//    map.addSurfacePoint(1500,100)
//    map.addSurfacePoint(3000,100)
//    map.addSurfacePoint(5000,1500)
//    map.addSurfacePoint(6999,1000)
//    CH2
//    MAP 1
//    map.addSurfacePoint(0, 100)
//    map.addSurfacePoint(1000, 1500)
//    map.addSurfacePoint(3000, 1000)
//    map.addSurfacePoint(4000, 150)
//    map.addSurfacePoint(5500, 150)
//    map.addSurfacePoint(6999, 800)
////    MAP 2
//    map.addSurfacePoint(0,100)
//    map.addSurfacePoint(1000,500)
//    map.addSurfacePoint(1500,100)
//    map.addSurfacePoint(3000,100)
//    map.addSurfacePoint(3500,500)
//    map.addSurfacePoint(3700,200)
//    map.addSurfacePoint(5000,1500)
//    map.addSurfacePoint(5800,300)
//    map.addSurfacePoint(6000,1000)
//    map.addSurfacePoint(6999,2000)
//    MAP 3
//    map.addSurfacePoint(0, 100)
//    map.addSurfacePoint(1000,500)
//    map.addSurfacePoint(1500,1500)
//    map.addSurfacePoint(3000,1000)
//    map.addSurfacePoint(4200,150)
//    map.addSurfacePoint(5500,150)
//    map.addSurfacePoint(6999,800)
//    MAP 4
//    map.addSurfacePoint(0,1000)
//    map.addSurfacePoint(300,1500)
//    map.addSurfacePoint(350,1400)
//    map.addSurfacePoint(500,2000)
//    map.addSurfacePoint(800,1800)
//    map.addSurfacePoint(1000,2500)
//    map.addSurfacePoint(1200,2100)
//    map.addSurfacePoint(1500,2400)
//    map.addSurfacePoint(2000,1000)
//    map.addSurfacePoint(2200,500)
//    map.addSurfacePoint(2500,100)
//    map.addSurfacePoint(2900,800)
//    map.addSurfacePoint(3000,500)
//    map.addSurfacePoint(3200,1000)
//    map.addSurfacePoint(3500,2000)
//    map.addSurfacePoint(3800,800)
//    map.addSurfacePoint(4000,200)
//    map.addSurfacePoint(5000,200)
//    map.addSurfacePoint(5500,1500)
//    map.addSurfacePoint(6999,2800)
//    MAP 5
//    map.addSurfacePoint(0,1000)
//    map.addSurfacePoint(300,1500)
//    map.addSurfacePoint(350,1400)
//    map.addSurfacePoint(500,2100)
//    map.addSurfacePoint(1500,2100)
//    map.addSurfacePoint(2000,200)
//    map.addSurfacePoint(2500,500)
//    map.addSurfacePoint(2900,300)
//    map.addSurfacePoint(3000,200)
//    map.addSurfacePoint(3200,1000)
//    map.addSurfacePoint(3500,500)
//    map.addSurfacePoint(3800,800)
//    map.addSurfacePoint(4000,200)
//    map.addSurfacePoint(4200,800)
//    map.addSurfacePoint(4800,600)
//    map.addSurfacePoint(5000,1200)
//    map.addSurfacePoint(5500,900)
//    map.addSurfacePoint(6000,500)
//    map.addSurfacePoint(6500,300)
//    map.addSurfacePoint(6999, 500)
// CH3
// MAP 1
//    map.addSurfacePoint(0,450)
//    map.addSurfacePoint(300,750)
//    map.addSurfacePoint(1000,450)
//    map.addSurfacePoint(1500,650)
//    map.addSurfacePoint(1800,850)
//    map.addSurfacePoint(2000,1950)
//    map.addSurfacePoint(2200,1850)
//    map.addSurfacePoint(2400,2000)
//    map.addSurfacePoint(3100,1800)
//    map.addSurfacePoint(3150,1550)
//    map.addSurfacePoint(2500,1600)
//    map.addSurfacePoint(2200,1550)
//    map.addSurfacePoint(2100,750)
//    map.addSurfacePoint(2200,150)
//    map.addSurfacePoint(3200,150)
//    map.addSurfacePoint(3500,450)
//    map.addSurfacePoint(4000,950)
//    map.addSurfacePoint(4500,1450)
//    map.addSurfacePoint(5000,1550)
//    map.addSurfacePoint(5500,1500)
//    map.addSurfacePoint(6000,950)
//    map.addSurfacePoint(6999,1750)
//    CH3 - MAP 2
    map.addSurfacePoint(0,1800)
    map.addSurfacePoint(300,1200)
    map.addSurfacePoint(1000,1550)
    map.addSurfacePoint(2000,1200)
    map.addSurfacePoint(2500,1650)
    map.addSurfacePoint(3700,220)
    map.addSurfacePoint(4700,220)
    map.addSurfacePoint(4750,1000)
    map.addSurfacePoint(4700,1650)
    map.addSurfacePoint(4000,1700)
    map.addSurfacePoint(3700,1600)
    map.addSurfacePoint(3750,1900)
    map.addSurfacePoint(4000,2100)
    map.addSurfacePoint(4900,2050)
    map.addSurfacePoint(5100,1000)
    map.addSurfacePoint(5500,500)
    map.addSurfacePoint(6200,800)
    map.addSurfacePoint(6999,600)

    map.addSurfacePoint(6999, -1)
    map.getLandingPoint()
//    val visualization = VisualDebugging()
//    visualization.loadMap(map)
    val search = Search()
    var initialized = false
//    while (true) {
//        CH1
//        val (x, y, hSpeed, vSpeed, fuel, angle, power) = listOf(2500, 2500, 0, 0, 500, 0, 0)
//        val (x, y, hSpeed, vSpeed, fuel, angle, power) = readln().split(" ").map { it.toInt() }
//        MAP 1
//        val (x, y, hSpeed, vSpeed, fuel, angle, power) = listOf(2500, 2700, 0, 0, 550, 0, 0)
//        MAP 2
//        val (x, y, hSpeed, vSpeed, fuel, angle, power) = listOf(6500, 2800, -100, 0, 600, 90, 0)
//        MAP 3
//        val (x, y, hSpeed, vSpeed, fuel, angle, power) = listOf(6500, 2800, -90, 0, 750, 90, 0)
//        MAP 4
//        val (x, y, hSpeed, vSpeed, fuel, angle, power) = listOf(500, 2700, 100, 0, 800, -90, 0)
//        MAP 5
//        val (x, y, hSpeed, vSpeed, fuel, angle, power) = listOf(6500, 2700, -50, 0, 1000, 90, 0)
//        CH3
//        MAP1
//        val (x, y, hSpeed, vSpeed, fuel, angle, power) = listOf(6500, 2600, -20, 0, 1000, 45, 0)
        val (x, y, hSpeed, vSpeed, fuel, angle, power) = listOf(6500, 2000, 0, 0, 1200, 0, 0)
//        println(map.inTerrain(2104, 295))
        val currentShip = Ship(x, y, hSpeed, vSpeed, angle, power, fuel)

//        val sim = Simulation(map, currentShip)
//        val ld = LanderSolution("69,2;28,3;3,2;23,3;17,4;-8,2;28,4;-2,4;-11,1;84,4;-10,4;-14,0;50,4;2,4;68,4;1,4;-13,2;-36,1;83,4;-10,4;16,4;61,0;6,4;-5,4;-21,4;48,1;90,4;4,4;78,0;40,0;10,3;21,4;7,3;49,4;6,4;13,4;90,1;21,3;6,4;6,4;57,2;15,4;5,4;-28,4;5,1;-40,4;-30,4;-23,4;24,4;17,0;-23,4;-26,4;-23,4;13,4;-22,4;-32,4;7,4;-10,3;-10,4;13,4;-10,3;-17,4;-9,4;-25,4;-1,4;-25,4;0,4;-27,4;-1,4;0,4;-9,4;-1,4;-24,3;2,4;4,4;-25,4;0,4;0,4;0,4;-1,4;4,4;-15,4;-14,4;0,4;0,4;-2,4;0,4;-12,3;-10,4;-9,4;-12,4;0,4;-12,3;0,4;-4,4;0,4;-8,4;15,4;0,4;5,3;0,4;0,4")
//
//        while (!sim.getResult().isFinish()) {
//            val st = ld.nextStepS().split(" ")
//            sim.nextState(st[0].toInt(), st[1].toInt())
//            println(sim.getResult(true))
//        }

//        if (!initialized) {
            System.err.println("Searching")
            val t = measureTimeMillis {
                search.init(map, currentShip)
                search.improve()
            }
            initialized = true
//        }
        System.err.println("Search time: $t ms")
        search.analytics()
        search.printSolutions()
        println("0 0")
//        readln()
//    }

}


fun main2() {
    val surfaceN = readln().toInt()
    for (i in 0 until surfaceN) {
        System.err.println(readln().split(" ").map{it.toInt()})
    }
    var search = LanderSolution("")
    var initialized = false
    while (true) {
        if (!initialized) {
//            search = LanderSolution("-9,4;-53,3;-58,4;-61,4;-52,4;-17,2;-28,4;-66,4;-63,0;-78,1;-66,4;35,4;-40,4;-33,4;-90,4;-78,4;14,0;63,4;-13,4;-10,4;-50,4;56,3;-20,4;62,4;7,4;7,2;36,2;52,3;-27,4;25,4;27,4;-35,4;27,4;48,4;25,4;-21,4;1,4;49,0;72,4;1,4;33,4;66,4;27,4;32,4;-6,4;-9,2;26,4;37,4;12,0;26,0;36,4;-15,0;10,4;26,4;1,4;0,4;1,4;0,4;11,4;8,4;0,4;0,4;14,4;0,4;-11,4;0,4;14,4;0,4;14,4;0,4;0,4;14,4;0,4;0,4;0,4;0,4;9,4;0,4;0,4;0,4;0,4;0,4;0,4;0,4;0,4")
//            search = LanderSolution("63,2;16,1;44,0;14,1;20,3;-20,4;-4,3;-56,4;-50,4;66,4;64,1;80,4;61,3;11,4;-13,4;-47,4;29,0;-21,1;62,3;-15,4;-23,4;-13,4;-41,4;90,4;-24,4;90,4;-16,4;-25,4;-25,4;-33,4;77,0;-15,4;-71,4;-55,4;-29,4;-48,4;-56,4;-56,4;-90,4;19,4;-79,4;3,4;-1,4;41,4;38,4;-23,4;-36,4;-29,4;-43,4;-21,4;-38,4;1,4;-25,4;-40,2;13,4;-39,4;0,4;-24,4;11,4;-26,4;0,4;-9,4;-9,4;-23,4;11,4;-10,4;19,4;-18,4;5,4;0,4;0,4;-13,4;6,4;-11,4;0,4;10,3;6,4;0,4;0,4;0,4;-5,4;-17,4;0,4;0,4;0,4;0,4;0,4;0,4;0,4;0,4")
//            search = LanderSolution("21,1;37,4;-20,0;-8,2;-44,2;-38,4;-61,4;-62,4;-75,4;-73,1;83,4;-56,4;-49,4;-41,4;-90,4;-90,4;44,4;-8,4;90,4;-81,4;-75,4;90,0;-18,4;29,0;-34,4;-74,4;44,4;59,4;-50,4;-50,4;65,4;-40,4;-55,4;73,1;43,4;90,2;90,4;-11,1;74,4;46,0;3,4;-11,4;-29,4;-29,4;-22,4;-28,4;-34,4;-19,4;-53,4;-36,4;21,4;-47,4;-34,4;17,4;-68,4;-20,4;-30,4;-18,4;-11,4;-22,4;-9,4;-1,4;46,3;0,4;-22,4;-22,4;13,4;-22,4;-7,4;-3,4;13,4;8,4;1,4;-12,4;-3,4;-15,4;-9,4;-15,4;-30,4;-15,4;0,4;0,4;0,4;0,4")
//            search = LanderSolution("-31,2;-20,2;-1,3;1,4;35,2;5,4;-15,4;-1,4;90,4;-62,0;25,4;60,3;27,4;-54,4;90,1;46,3;-16,4;4,4;54,4;39,0;-85,4;33,3;57,4;54,1;-56,4;-59,4;-65,4;-55,4;16,4;59,4;71,3;39,4;-44,4;50,4;-31,4;63,0;68,4;80,4;-74,2;-58,4;3,1;62,1;19,3;83,4;-57,4;-35,4;43,4;34,0;79,4;58,1;30,4;-82,4;-35,4;-32,4;29,3;-21,4;22,4;23,4;15,4;39,4;23,4;36,4;8,4;48,4;-4,4;11,4;26,4;34,4;15,4;28,4;27,4;30,4;30,4;30,4;30,4;30,4;30,4;30,4;30,4;30,4;30,4;15,4;15,4;15,4;15,4;15,4;15,4;0,4;0,4;0,4")
            search = LanderSolution("56,0;3,0;40,0;14,3;90,2;-10,1;12,1;66,2;9,3;90,3;9,1;64,4;11,4;-8,4;-19,4;-9,4;-8,4;-40,4;-41,3;82,4;-25,4;76,4;35,4;-45,4;-28,4;-51,3;-44,4;-48,4;6,4;-52,4;-56,4;-73,4;-11,4;11,4;41,4;-23,4;5,4;-23,4;-8,4;-23,4;-3,4;-25,4;0,4;11,4;0,4;0,4;0,4;-21,4;-13,4;0,4;0,4;0,4;0,4;0,4;0,4;0,4;0,4")

            // MAP 1 - CH3
            // search = LanderSolution("57,1;59,4;13,3;48,3;33,2;15,3;75,4;35,4;15,4;21,1;30,4;42,1;50,3;26,4;72,1;25,4;8,1;1,3;54,4;47,0;30,3;13,4;58,4;57,4;35,4;33,4;13,0;-11,3;-11,4;-44,2;36,4;-29,4;-26,4;69,4;-22,4;-10,4;29,4;-32,2;20,4;-26,4;4,4;-29,4;10,1;-6,4;-16,4;53,0;-20,4;-34,1;45,4;-21,4;-30,1;33,4;-13,4;-36,4;-24,4;-68,4;39,4;-28,1;-36,3;0,4;0,4;0,4;-15,4;-12,4;0,4;6,4;-16,4;0,4;0,4;-13,4;-14,4;8,4;16,4;-20,4;0,4;-15,4;-15,4;-15,4;-6,4;-30,4;-15,4;-15,4;-15,4;-12,3;-15,4;-1,4;-15,4;1,4;-15,4;0,4")

            initialized = true
        }
        readln()
        readln()
        System.err.println("Next step")
        search.nextStep()
    }
}