import kotlin.math.hypot

private operator fun <E> List<E>.component6(): E = this[5]
private operator fun <E> List<E>.component7(): E = this[6]

data class SurfaceMap(
    private val points: MutableList<Pair<Int, Int>> = mutableListOf(),
) {
    fun addSurfacePoint(x: Int, y: Int) = points.add(x to y)
    fun getLandingPoint(): Pair<Int, Int> {
        var foundPoint = -1 to -1
        for (i in 0 until points.size - 1) {
            if (foundPoint.first != -1 && foundPoint.second != -1) break
            val pointA = points[i]
            val pointB = points[i + 1]
            if (pointA.second == pointB.second) foundPoint = (pointA.first + pointB.first) / 2 to pointA.second
        }
        return foundPoint
    }

    fun getEdges(): MutableList<Pair<Pair<Int, Int>, Pair<Int, Int>>> {
        val edges = mutableListOf<Pair<Pair<Int, Int>, Pair<Int, Int>>>()
        for (i in 0 until points.size - 1) {
            edges.add(points[i] to points[i+1])
        }
        edges.add(points[points.size - 1] to points[0])
        return edges
    }

    fun getLandingPoints(): Pair<Pair<Int, Int>, Pair<Int, Int>> {
        var pointA: Pair<Int, Int>? = null
        var pointB: Pair<Int, Int>? = null

        for (p in 0 until points.size - 1) {
            if (pointA != null) break
            val checkedA = points[p]
            val checkedB = points[p + 1]
            if (checkedA.second == checkedB.second) {
                pointA = checkedA
                pointB = checkedB
            }
        }

        return pointA!! to pointB!!
    }

    fun inTerrain(x: Int, y: Int): Boolean {
        var count = 0
        val edges = getEdges()

        for (e in edges) {
            val (x1, y1) = e.first
            val (x2, y2) = e.second

            if ((y < y1) != (y < y2) && x < x1 + ((y - y1).toDouble() / (y2 - y1))*(x2-x1)) count += 1
        }

        return count % 2 == 1
    }
}

class Solutions {
    private val solutions = mapOf(
        1 to LanderSolution("36,1;40,3;29,4;-4,4;9,4;46,0;-52,4;56,4;6,4;90,0;12,4;26,1;12,4;7,4;72,4;-1,4;-18,4;82,1;-8,0;-17,3;-27,4;-28,4;90,4;65,0;81,0;80,2;73,2;37,3;17,4;90,0;18,4;67,0;37,2;23,3;36,4;90,0;29,4;13,4;-1,4;-2,4;-14,4;-40,4;-26,4;-25,4;-32,2;-36,4;6,4;-46,4;-33,4;-63,4;-31,4;-32,4;-35,4;-64,4;17,4;-30,3;-42,4;17,4;-28,4;-23,4;-29,4;-61,4;-21,4;42,4;-33,4;-45,4;-20,4;-28,4;-49,4;10,4;-39,4;0,4;9,4;-29,4;-27,4;-24,4;2,4;-2,4;-10,4;-8,4;-9,4;-14,4;0,4;0,4;0,4;0,4;0,4;0,4;0,4;0,4;0,4;0,4;0,4;0,4;0,4"),
        2 to LanderSolution("-9,4;-53,3;-58,4;-61,4;-52,4;-17,2;-28,4;-66,4;-63,0;-78,1;-66,4;35,4;-40,4;-33,4;-90,4;-78,4;14,0;63,4;-13,4;-10,4;-50,4;56,3;-20,4;62,4;7,4;7,2;36,2;52,3;-27,4;25,4;27,4;-35,4;27,4;48,4;25,4;-21,4;1,4;49,0;72,4;1,4;33,4;66,4;27,4;32,4;-6,4;-9,2;26,4;37,4;12,0;26,0;36,4;-15,0;10,4;26,4;1,4;0,4;1,4;0,4;11,4;8,4;0,4;0,4;14,4;0,4;-11,4;0,4;14,4;0,4;14,4;0,4;0,4;14,4;0,4;0,4;0,4;0,4;9,4;0,4;0,4;0,4;0,4;0,4;0,4;0,4;0,4"),
        3 to LanderSolution("63,2;16,1;44,0;14,1;20,3;-20,4;-4,3;-56,4;-50,4;66,4;64,1;80,4;61,3;11,4;-13,4;-47,4;29,0;-21,1;62,3;-15,4;-23,4;-13,4;-41,4;90,4;-24,4;90,4;-16,4;-25,4;-25,4;-33,4;77,0;-15,4;-71,4;-55,4;-29,4;-48,4;-56,4;-56,4;-90,4;19,4;-79,4;3,4;-1,4;41,4;38,4;-23,4;-36,4;-29,4;-43,4;-21,4;-38,4;1,4;-25,4;-40,2;13,4;-39,4;0,4;-24,4;11,4;-26,4;0,4;-9,4;-9,4;-23,4;11,4;-10,4;19,4;-18,4;5,4;0,4;0,4;-13,4;6,4;-11,4;0,4;10,3;6,4;0,4;0,4;0,4;-5,4;-17,4;0,4;0,4;0,4;0,4;0,4;0,4;0,4;0,4"),
        4 to LanderSolution("21,1;37,4;-20,0;-8,2;-44,2;-38,4;-61,4;-62,4;-75,4;-73,1;83,4;-56,4;-49,4;-41,4;-90,4;-90,4;44,4;-8,4;90,4;-81,4;-75,4;90,0;-18,4;29,0;-34,4;-74,4;44,4;59,4;-50,4;-50,4;65,4;-40,4;-55,4;73,1;43,4;90,2;90,4;-11,1;74,4;46,0;3,4;-11,4;-29,4;-29,4;-22,4;-28,4;-34,4;-19,4;-53,4;-36,4;21,4;-47,4;-34,4;17,4;-68,4;-20,4;-30,4;-18,4;-11,4;-22,4;-9,4;-1,4;46,3;0,4;-22,4;-22,4;13,4;-22,4;-7,4;-3,4;13,4;8,4;1,4;-12,4;-3,4;-15,4;-9,4;-15,4;-30,4;-15,4;0,4;0,4;0,4;0,4"),
        5 to LanderSolution("-31,2;-20,2;-1,3;1,4;35,2;5,4;-15,4;-1,4;90,4;-62,0;25,4;60,3;27,4;-54,4;90,1;46,3;-16,4;4,4;54,4;39,0;-85,4;33,3;57,4;54,1;-56,4;-59,4;-65,4;-55,4;16,4;59,4;71,3;39,4;-44,4;50,4;-31,4;63,0;68,4;80,4;-74,2;-58,4;3,1;62,1;19,3;83,4;-57,4;-35,4;43,4;34,0;79,4;58,1;30,4;-82,4;-35,4;-32,4;29,3;-21,4;22,4;23,4;15,4;39,4;23,4;36,4;8,4;48,4;-4,4;11,4;26,4;34,4;15,4;28,4;27,4;30,4;30,4;30,4;30,4;30,4;30,4;30,4;30,4;30,4;30,4;15,4;15,4;15,4;15,4;15,4;15,4;0,4;0,4;0,4"),
        6 to LanderSolution(""),
        7 to LanderSolution("57,1;59,4;13,3;48,3;33,2;15,3;75,4;35,4;15,4;21,1;30,4;42,1;50,3;26,4;72,1;25,4;8,1;1,3;54,4;47,0;30,3;13,4;58,4;57,4;35,4;33,4;13,0;-11,3;-11,4;-44,2;36,4;-29,4;-26,4;69,4;-22,4;-10,4;29,4;-32,2;20,4;-26,4;4,4;-29,4;10,1;-6,4;-16,4;53,0;-20,4;-34,1;45,4;-21,4;-30,1;33,4;-13,4;-36,4;-24,4;-68,4;39,4;-28,1;-36,3;0,4;0,4;0,4;-15,4;-12,4;0,4;6,4;-16,4;0,4;0,4;-13,4;-14,4;8,4;16,4;-20,4;0,4;-15,4;-15,4;-15,4;-6,4;-30,4;-15,4;-15,4;-15,4;-12,3;-15,4;-1,4;-15,4;1,4;-15,4;0,4"),
        8 to LanderSolution("")
    )

    fun getRequiredSolution(point: Int) {

    }
}

class LanderSolution(solution: String) {
    private val solutionStep: MutableList<String> = mutableListOf()
    private var currentStep = 0

    init {
        solutionStep.addAll(solution.split(";").toMutableList())
    }

    fun nextStep() {
        val step = if (currentStep < solutionStep.size) currentStep else solutionStep.size - 1
        val msg = solutionStep[step].replace(",", " ")
        currentStep += 1
        println(msg)
    }

    fun nextStepS(): String {
        val step = if (currentStep < solutionStep.size) currentStep else solutionStep.size - 1
        val msg = solutionStep[step].replace(",", " ")
        currentStep += 1
        return msg
    }
}

fun main() {
    val surfaceN = readln().toInt()
    val mp = SurfaceMap()
    repeat(surfaceN) {
        val (landX, landY) = readln().split(" ").map { it.toInt() }
        mp.addSurfacePoint(landX, landY)
    }
    var search = LanderSolution("")
    var initialized = false
    while (true) {
        val (x, y, xs, ys, fuel, rotation, power) = readln().split(" ").map{it.toInt()}
        val newRotation = calculateRotation(x, y, xs, ys, fuel, rotation, power, mp, listOf())
        val thrust = calculateThrust(x, y, xs, ys, fuel, rotation, power, mp, listOf())

        println("$newRotation $thrust")

//        if (!initialized) {
//            search = LanderSolution("75,2;-22,2;-27,3;-53,4;-52,4;88,4;-68,4;66,4;31,4;33,4;34,4;-64,4;41,4;-7,3;51,4;31,4;-35,4;43,4;30,4;19,4;90,4;-53,4;82,4;-68,4;-90,4;0,4;84,4;16,4;60,4;-81,3;-80,4;-70,4;5,4;-15,4;-34,4;-31,4;-13,3;66,4;0,3;65,4;-28,4;-2,3;5,4;-42,4;81,4;73,4;-74,4;-59,4;67,4;-39,4;60,4;-41,4;7,4;-86,4;32,4;-32,4;-20,4;-35,4;-26,4;-25,4;58,4;-62,4;-27,4")
//            initialized = true
//        }
//
//        search.nextStep()
    }
}

fun calculateRotation(x:Int, y:Int, xs:Int, ys:Int, fuel:Int, rotation:Int, power:Int, map: SurfaceMap, params: List<Int>): Int {
    val normalizedX = x/7000
    val normalizedY = y/3000
    val sum1 = x * params[0] + y * params[1] + xs * params[2] + ys * params[3] + fuel * params[4] + rotation * params[5] + power * params[6]
    val sum2 = x * x * params[7] + y * y * params[8] + xs * xs * params[9] + ys * ys * params[10] + fuel * fuel * params[11] + rotation * rotation * params[12] + power * power * params[13]
    val sum3 = x*x*x*params[14] + y*y*y*params[15] +
            xs*xs*xs*params[16] + ys*ys*ys*params[17] +
            fuel*fuel*fuel*params[18] + rotation*rotation*rotation*params[19] +
            power*power*power*params[20]

    return sum1 + sum2 + sum3

//    var ed = ((0 to 0) to (0 to 0)) to Int.MAX_VALUE
//
//    for (i in map.getEdges()) {
//        val xP = (i.first.first + i.second.first) / 2
//        val yP = (i.first.second + i.second.second) / 2
//        val d = hypot((xP - x).toDouble(), (yP - y).toDouble())
//        if (d < ed.second) ed = (i.first to i.second) to d.toInt()
//    }
}

fun calculateThrust(x:Int, y:Int, xs:Int, ys:Int, fuel:Int, rotation:Int, power:Int, map: SurfaceMap, params: List<Int>): Int {
    val sum1 = x * params[21] + y * params[22] + xs * params[23] + ys * params[24] + fuel * params[25] + rotation * params[26] + power * params[27]
    val sum2 = x * x * params[28] + y * y * params[29] + xs * xs * params[30] + ys * ys * params[31] + fuel * fuel * params[32] + rotation * rotation * params[33] + power * power * params[34]
    val sum3 = x*x*x*params[35] + y*y*y*params[36] +
            xs*xs*xs*params[37] + ys*ys*ys*params[38] +
            fuel*fuel*fuel*params[39] + rotation*rotation*rotation*params[40] +
            power*power*power*params[41]

    return sum1 + sum2 + sum3
}